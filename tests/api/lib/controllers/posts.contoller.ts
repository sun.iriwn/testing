import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(authorIdValue: number, previewImageValue: string, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorIdValue,
                previewImage: previewImageValue,
                body: bodyValue
              })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLike(entityIdValue:number, isLikeValue: boolean, userIdValue: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                entityId: entityIdValue,
                isLike: isLikeValue,
                userId: userIdValue
              })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
