import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async registretion(idValue: number, avatarValue: string, emailValue: string, userNameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                id: idValue,
                avatar: avatarValue,
                email: emailValue,
                userName: userNameValue,
                password: passwordValue
              })
            .send();
        return response;
    }
}
