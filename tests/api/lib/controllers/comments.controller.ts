import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class CommentsController {
    async addComment(authorIdValue: number, postIdValue: number, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId: authorIdValue,
                postId: postIdValue,
                body: bodyValue
              })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
