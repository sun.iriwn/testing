import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.conroller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();

describe("Create new user", () => {
    it(`Create new user`, async () => {
        let response = await register.registretion(0, "string", "sun@ukr.net", "Iryna", "123456");
        
        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response,5000);
    });
    
    after(function(){
        console.log(`It was a test`);
    });
});