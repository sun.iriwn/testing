import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.contoller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
const posts = new PostsController();
const auth = new AuthController();

describe(`Get all posts`, () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });
   
    it(`should return 200 status code and all posts`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
    });
    
    afterEach(function(){
        console.log(`It was a test`);
    });
});