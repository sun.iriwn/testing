import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.contoller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
const posts = new PostsController();
const auth = new AuthController();

describe("Create new post", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });

    it(`Create new post`, async () => {
        let response = await posts.createPost(0, "image_BSA", "BSA is cool", accessToken);
        
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });
    
    after(function(){
        console.log(`It was a test`);
    });
});