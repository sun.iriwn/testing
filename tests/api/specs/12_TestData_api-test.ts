import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data set for login', () => {
    let invalidCredentialsDataSet = [
        { email: 'sun@ukr.net', password: '' },
        { email: 'sun@ukr.net', password: '      ' },
        { email: 'sun@ukr.net', password: '123456 ' },
        { email: 'sun@ukr.net', password: 'A 123455' },
        { email: 'sun@ukr.net', password: 'A*****' },
        { email: 'sun@ukr.net', password: 'яяяяяяя' },
        { email: 'sun@ukr.net', password: 'admin' },
        { email: 'sun@ukr.net', password: 'sun@ukr.net' }
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });
});
