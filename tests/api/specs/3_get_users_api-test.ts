import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
const users = new UsersController();


describe(`Users controller`, () => {
    let userId: number;
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);  
        
        userId = response.body[1].id;
    });
    it(`should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 555555
        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 404);
        checkResponseTime(response,3000); 
    });
    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = '2121838367671313212183333'
        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 400);
        checkResponseTime(response,3000); 
    });
    
    afterEach(function(){
        console.log(`It was a test`);
    });
});