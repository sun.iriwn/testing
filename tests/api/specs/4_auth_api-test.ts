import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;
        //console.log(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 4365,
            avatar: "string",
            email: "sun@ukr.net",
            userName: "string",
        };
        let response = await users.updateUser(userData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,5000);
    });
    
    after(function(){
        console.log(`It was a test`);
    });
});
