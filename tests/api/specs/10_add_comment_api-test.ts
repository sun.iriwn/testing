import { expect } from "chai";
import { CommentsController } from "../lib/controllers/comments.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
const comments = new CommentsController();
const auth = new AuthController();

describe("Add comment to post", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });

    it(`Add comment to post`, async () => {
        let response = await comments.addComment(0, 3426, "I agree", accessToken);
        
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });
    
    after(function(){
        console.log(`It was a test`);
    });
});