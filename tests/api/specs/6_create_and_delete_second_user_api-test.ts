import { expect, use } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.conroller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const register = new RegisterController();

describe("Delete user", () => {
    let accessToken: string;
    let userId: number;


    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });

    it(`Create new user`, async () => {
        let response = await register.registretion(0, "string", "sun@ukr.net", "Iryna", "123456");
        
        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response,5000);

        userId = response.body.user.id;
    });

    it(`Delete user`, async () => {
        let response = await users.deleteUserById(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,5000);
    });
});