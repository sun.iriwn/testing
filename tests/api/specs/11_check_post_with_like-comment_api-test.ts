import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.contoller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
const posts = new PostsController();
const auth = new AuthController();

describe(`Check post with like and comment`, () => {
    let accessToken: string;
    let postData: object;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
    });

    it(`should return 200 status code and post by id`, async () => {
       let response = await posts.getAllPosts();

       let post;
       for(let i = 0; i < response.body.length; i++) {
            if(response.body[i].id == 3426) {
            post = response.body[i];
            break;
            }
        }
        console.log(post);

        checkStatusCode(response, 200);
        checkResponseTime(response,3000); 
    });

    afterEach(function(){
        console.log(`It was a test`);
    });
});